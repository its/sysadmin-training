Wednesday, February 1, 2017

---

# SysAdmin Training Overview 
  - https://goo.gl/9X3fCH 

---

# Tickets
  - n/a

---

# Review
## Confluence Wiki
## Google Docs

---

# Provisioning 

## VMware vSphere
  - https://vsphere.ncsa.illinois.edu/vsphere-client/
  - https://wiki.ncsa.illinois.edu/display/ITS/Using+ITS+vSphere

## RHEL / CentOS
  - https://www.centos.org/about/
  - https://access.redhat.com/documentation/en-US/Red_Hat_Enterprise_Linux/7/html/Installation_Guide/part-installation-intel-amd.html

---

# Tasks
  - setup test VM(s) for ldap testing (Cameron: ldap-test1.ncsa.illinois.edu, Shaq: ldap-test2.ncsa.illinois.edu)
    - request a hostname and IP from NetEng (see https://goo.gl/mmcyMz)
    - create the VM (see https://goo.gl/PMiJ76)
      - add the VM to the `NCSA ITS` > `ITS` folder
    - add the VM to Foreman in the `ITS` group (https://goo.gl/z9kQco)
    - add kerberos host principal keytab (https://goo.gl/pSh6Ao)
    - ssh into your VM and look around
  - watch https://youtu.be/kuJWIs0EKQs for vSphere overview (~17min)
  - download the latest CentOS 7.x minimal ISO
    - upload it to vSphere's `install-media` datastore
