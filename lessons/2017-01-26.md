Thursday, January 26, 2017

# SysAdmin Training Overview 
  - https://goo.gl/9X3fCH 

# Tickets


# Review
## git
## bash
## cerberus
## public-linux
## its-foreman

# Communication
  - NCSA has thousands of users who don't have illinois.edu accounts
    - software licensing and account management issues

## Chat (Rocket.chat)
  - https://chat.ncsa.illinois.edu/
  - multiplatform, highly configurable, open source
  - messaging, search, archiving & custom integrations
  - mirrors modern features of Slack or HipChat

## Jabber
  - https://wiki.ncsa.illinois.edu/display/ITS/Jabber+Chat+Service
  - instant messaging and group chat

## RT
  - https://help.ncsa.illinois.edu/
  - NCSA's default help desk ticketing system

## JIRA
  - https://jira.ncsa.illinois.edu/
  - bug tracking, issue tracking, and project management system
  - mainly used by specific projects (e.g. BW, LSST)

# Tasks
  - setup a chat client for **NCSA Jabber**
    - connect to a group chat, e.g. `its` or `storage`
  - connect to **NCSA Chat server**
    - send to direct messages to each other
    - review https://rocket.chat/docs/user-guides
    - read about building custom integrations:
      - https://rocket.chat/docs/administrator-guides/integrations
      - https://wiki.ncsa.illinois.edu/display/ITS/NCSA+Chat
  - connect to **NCSA RT**
    - look at `RT Essentials` from Safari Online: https://goo.gl/wtVHo0
    - create an RT search to look for tickets you've worked on
    - add yourself as an AdminCC to an open ticket so that you can watch it
  - connect to **NCSA Jira**
    - review https://confluence.atlassian.com/get-started-with-jira-software/
    - figure out which projects you have access to
    - set yourself as a watcher on an open ticket/issue


