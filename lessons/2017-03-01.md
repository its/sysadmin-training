Wednesday, March 1, 2017

---

# SysAdmin Training Overview 
  - https://goo.gl/9X3fCH 

---

# Tickets
  - Unowned Tickets
    - Down from 60ish to 26 - https://goo.gl/m7fLgW
  - Stalled Tickets
    - Approx 90 - https://goo.gl/jxf8mn

---

# Migrating off EL5 OS

## Red Hat Enterprise Linux (rhel) 5 (and thus CentOS 5) will be retired on March 31, 2017
  - https://rhn.redhat.com/errata/RHSA-2016-0561.html
  - https://wiki.centos.org/FAQ/General#head-fe8a0be91ee3e7dea812e8694491e1dde5b75e6d
  - https://wiki.ncsa.illinois.edu/display/ITS/Old+Linux+Servers+Needing+Upgrade

---

# Tasks

  - Migrate astro to clone of ncsa30 (EL7)
    - see https://wiki.ncsa.illinois.edu/display/ITS/Creating+a+WordPress+Website+at+NCSA
  - Migrate lait to clone of ncsa30 (EL7)
  - Setup new FTP server with vsftpd and move www-vm03 sites to it
  - Migrate old websites from www-vm* to new EL7 servers (web[1-5])
    - See https://wiki.ncsa.illinois.edu/display/ITS/Web+Servers+and+their+Hosted+Websites
  - Rebuild AFS db servers on EL7

